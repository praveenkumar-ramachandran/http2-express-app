var express = require('express');
var path = require('path');
var router = express.Router();

/* GET profile */
router.get('/', function (req, res, next) {
  // res.send('Profile in routes is called');
  res.sendFile(path.resolve(__dirname + '/../public/profile-page.html'));
});

module.exports = router;
