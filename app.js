var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var profileRouter = require("./routes/profile");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// add static paths
app.use(express.static(path.join(__dirname, "/public")));
app.use("/html", express.static(path.join(__dirname, "/public/html")));
app.use('/javascripts', express.static(path.join(__dirname, "/public/javascripts")));
app.use('/stylesheets', express.static(path.join(__dirname, "/public/stylesheets")));
app.use('/images', express.static(path.join(__dirname, "/public/images")));

// add routers
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/profile", profileRouter);

const fs = require('fs');
var https = require("https");
// adding certificates
const options = {
  key: fs.readFileSync('./server.key'),
  cert: fs.readFileSync('./server.crt'),
  // allowHTTP1: true
};

// add HTTPS to HTTP/1.1
https.createServer(options, app)
  .listen(8080);

// Add HTTP/2 API
// reference : https://www.npmjs.com/package/http2
const http2 = require('http2');
const mime = require('mime-types');


const serverRoot = "./public";

// Function to handle HTTP Error
function respondToStreamError(err, stream) {
  console.log(err);
  if (err.code === 'ENOENT') {
    stream.respond({ ":status": HTTP_STATUS_NOT_FOUND });
  } else {
    stream.respond({ ":status": HTTP_STATUS_INTERNAL_SERVER_ERROR });
  }
  stream.end();
}

const {
  HTTP2_HEADER_PATH,
  HTTP2_HEADER_METHOD,
  HTTP_STATUS_NOT_FOUND,
  HTTP_STATUS_INTERNAL_SERVER_ERROR
} = http2.constants;

// create a secured server
http2.createSecureServer(options)
  .on('stream', (stream, headers) => {
    const reqPath = headers[HTTP2_HEADER_PATH];
    const reqMethod = headers[HTTP2_HEADER_METHOD];

    const fullPath = path.join(serverRoot, reqPath);
    const responseMimeType = mime.lookup(fullPath);

    stream.respondWithFile(fullPath, {
      'content-type': responseMimeType
    }, {
      onError: (err) => respondToStreamError(err, stream)
    });

  }).listen(8443);

http2.createSecureServer(options)
  .on('stream', (stream, headers) => {
    const reqPath = headers[HTTP2_HEADER_PATH];
    const reqMethod = headers[HTTP2_HEADER_METHOD];

    const fullPath = path.join(serverRoot, reqPath);
    const responseMimeType = mime.lookup(fullPath);

    if (fullPath.endsWith(".html")) {
      console.log(' **** html ****');
      // handle HTML file
      stream.respondWithFile(fullPath, {
        "content-type": "text/html"
      }, {
        onError: (err) => {
          respondToStreamError(err, stream);
        }
      });

      stream.pushStream({ ":path": "/" },
        { parent: stream.id },
        (err, pushStream, headers) => {
          console.log('pushing');
          pushStream.respondWithFile(path.join(serverRoot, "/javascripts/profile-page.js"),
            { 'content-type': "application/javascript" },
            {
              onError: (err) => {
                respondToStreamError(err, pushStream);
              }
            });
          pushStream.respondWithFile(path.join(serverRoot, "/stylesheets/style.css"),
            { 'content-type': "text/css" },
            {
              onError: (err) => {
                respondToStreamError(err, pushStream);
              }
            });
          pushStream.respondWithFile(path.join(serverRoot, "/stylesheets/style1.css"),
            { 'content-type': "text/css" },
            {
              onError: (err) => {
                respondToStreamError(err, pushStream);
              }
            });
          pushStream.respondWithFile(path.join(serverRoot, "/stylesheets/style2.css"),
            { 'content-type': "text/css" },
            {
              onError: (err) => {
                respondToStreamError(err, pushStream);
              }
            });
        });

    } else {
      // handle static file
      console.log(fullPath);
      stream.respondWithFile(fullPath, {
        'content-type': responseMimeType
      }, {
        onError: (err) => respondToStreamError(err, stream)
      });
    }

  }).listen(8444);


module.exports = app;
